local Moan = require("libraries.moan")
local Camera = require("libraries.camera")
local Sti = require("libraries.sti")
local Game
do
  local _class_0
  local state
  local _base_0 = {
    player_name = "Kasey",
    msg = "hewwo <3",
    load = function(self, u)
      u.map = require("./maps/opening.js")
    end,
    update = function(self, dt) end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function() end,
    __base = _base_0,
    __name = "Game"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  local self = _class_0
  state = {
    playing = playing,
    dead = dead,
    menu = menu
  }
  Game = _class_0
end
love.keyreleased = function(key)
  Moan.keyreleased(key)
  if key == "f" then
    return Moan.advanceMsg()
  elseif key == "`" then
    Moan.debug = not Moan.debug
  elseif key == "c" then
    return Moan.clearMessages()
  end
end
do
  local _with_0 = love
  _with_0.load = function()
    Moan.font = love.graphics.newFont("assets/Pixel UniCode.ttf", 32)
    local JPfallback = love.graphics.newFont("assets/JPfallback.ttf", 32)
    Moan.font:setFallbacks(JPfallback)
    Moan.typeSound = love.audio.newSource("assets/typeSound.wav", "static")
    Moan.optionOnSelectSound = love.audio.newSource("assets/optionSelect.wav", "static")
    Moan.optionSwitchSound = love.audio.newSource("assets/optionSwitch.wav", "static")
    love.graphics.setBackgroundColor(0.39, 0.39, 0.39)
    math.randomseed(os.time())
    local p1 = {
      x = 100,
      y = 200
    }
    local p2 = {
      x = 400,
      y = 150
    }
    local p3 = {
      x = 200,
      y = 300
    }
    local camera = Camera(p1.x, p1.y)
    Moan.setCamera(camera)
    local kasey = love.graphics.newImage("./assets/kasey.jpg")
    local hana = love.graphics.newImage("./assets/hana.jpg")
    do
      Moan.speak({
        "Kasey",
        {
          255,
          105,
          180
        }
      }, {
        "u-uhm.. hiii <3",
        "*shits the bed violently*"
      }, {
        image = kasey
      })
      Moan.speak({
        "Hana",
        {
          255,
          105,
          180
        }
      }, {
        "kys"
      }, {
        onstart = function(self)
          return Moan.setSpeed("slow")
        end
      }, {
        image = hana
      })
    end
    local main_bg = _with_0.graphics.newImage("./assets/base.jpg")
  end
  _with_0.update = function(dt)
    return Moan.update(dt)
  end
  _with_0.draw = function()
    return Moan.draw()
  end
  return _with_0
end
