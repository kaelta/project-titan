
----------------------
-- Splash Gamestate --
----------------------

Splash = {}
centerX, centerY = love.graphics.getWidth()/2, love.graphics.getHeight()/2

Splash\enter = ->
  self.duration = 5

Splash\update = (dt) ->
  self.duration = self.duration - dt
  if self.duration < 0
    Gamestate.switch(Menu)

Splash\draw = ->
  love.graphics.setColor 150, 110, 90, 255/self.duration
  love.graphics.rectangle "fill", centerX-32, centerY-32, 64, 64
  love.graphics.setColor 255, 255, 255, 255/self.duration
  love.graphics.print "SPLAASH LOGOO", centerX-32, centerY+32


Splash\keypressed = (key) ->
  Gamestate.switch Menu if key

Splash

